<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
/**
 * arrays that are mass assignable when inserting/updating the customer model
 * @var [type]
 */
    protected $fillable = ['name', 'email', 'contact_number', 'username', 'password', 'user_id'];

/**
 * one to one relation between customer and user model
 * @return instance of user model
 */
    public function users() {
      return $this->belongsTo('App\Models\User');
    }

/**
 * one to many relation between customer and customer_booking model
 * @return instance of customer_booking model
 */
    public function customerBookings() {
      return $this->hasMany('App\Models\Customer_booking');
    }

/**
 * one to many relation between customer and photographer_review model
 * @return instance of photographer_review model
 */
    public function photographerReviews() {
      return $this->hasMany('App\Models\Photographer_review');
    }

/**
 * one to many relation between customer and photographer_rating model
 * @return instance of photographer_rating model
 */
    public function photographerRatings() {
      return $this->hasMany('App\Models\Photographer_rating');
    }
}
