<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photographer_review extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating photographer_review model
   * @var arrays
   */
    protected $fillable = ['customer_id', 'photographer_id', 'review_msg', 'date'];

/**
 * one to many relation between customer and photographer_review
 * @return instance of customer model
 */
    public function customers() {
      return $this->belongsTo('App\Models\Customer');
    }

/**
 * one to many relation between photographer and photographer_review
 * @return instance of photographer model
 */
    public function photographers() {
      return $this->belongsTo('App\Models\Photographer');
    }
}
