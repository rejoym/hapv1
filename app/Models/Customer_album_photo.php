<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_album_photo extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating customer_album_photo
   * @var arrays
   */
    protected $fillable = ['customer_album_id', 'photo_url', 'photo_name', 'date'];

/**
 * [customerAlbum description]
 * @return [type] [description]
 */
    public function customerAlbum() {
      return $this->belongsTo('App\Models\Customer_album');
    }

}
