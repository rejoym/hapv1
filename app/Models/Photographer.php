<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photographer extends Model
{
  /**
   * arrays that are mass assignable while inserting/updating in the photographer table
   * @var array
   */
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'country', 'city', 'address', 'gender', 'contact_number', 'email', 'date_of_birth', 'identity_pic', 'award_pic', 'profile_pic', 'profile_pic', 'username', 'password', 'payment_info', 'bank_info', 'bank_account_no', 'user_id'];

/**
 * one to one relation between photographer and user model
 * @param none
 * @return instance of user model
 */
    public function users() {
      return $this->belongsTo('App\Models\User');
    }

/**
 * one to many relation between photographer and photographer_prices model
 * @param none
 * @return instance of photographer_price model
 */
    public function photographerPrices() {
      return $this->hasMany('App\Models\Photographer_price');
    }

/**
 * [photographerReviews description]
 * @return [type] [description]
 */
    public function photographerReviews() {
      return $this->hasMany('App\Models\Photographer_review');
    }

/**
 * [photographerRatings description]
 * @return [type] [description]
 */
    public function photographerRatings() {
      return $this->hasMany('App\Models\Photographer_rating');
    }

/**
 * [bookingPayments description]
 * @return [type] [description]
 */
    public function bookingPayments() {
      return $this->hasMany('App\Models\Booking_payment');
    }

/**
 * [genrePhotographers description]
 * @return [type] [description]
 */
    public function genrePhotographers() {
      return $this->hasMany('App\Models\Genre_photographer');
    }

/**
 * [customerAlbums description]
 * @return [type] [description]
 */
    public function customerAlbums() {
      return $this->hasMany('App\Models\Customer_album');
    }

/**
 * [customerBookingPhotographers description]
 * @return [type] [description]
 */
    public function customerBookingPhotographers() {
      return $this->hasMany('App\Models\Customer_booking_photographer');
    }
}
