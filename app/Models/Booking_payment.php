<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking_payment extends Model
{
/**
 * arrays that are mass assignable when inserting/updating Booking_payment model
 * @var array
 */
    protected $fillable = ['customer_booking_id', 'photographer_id', 'payment_info', 'paid_amount', 'paid_amount', 'date', 'payment_via'];

/**
 * [photographers description]
 * @return [type] [description]
 */
    public function photographers() {
      return $this->belongsTo('App\Models\Photographer');
    }

/**
 * [customerBookings description]
 * @return [type] [description]
 */
    public function customerBookings() {
      return $this->belongsTo('App\Models\Customer_booking');
    }
}
