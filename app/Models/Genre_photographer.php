<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre_photographer extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating genre_photographer model
   * @var array
   */
    protected $fillable = ['genre_id', 'photographer_id'];

/**
 * one to many relation between genre and genre_photographer
 * @return instance of genre
 */
    public function genres() {
      return $this->belongsTo('App\Models\Genre');
    }

/**
 * one to many relation between genre_photographer and genre_photographer_photo
 * @return instance of genre_photographer_photo
 */
    public function genrePhotographerPhotos() {
      return $this->hasMany('App\Models\Genre_photographer_photo');
    }

/**
 * one to many relation between photographer and genre_photographer
 * @return instance of photographer
 */
    public function photographers() {
      return $this->belongsTo('App\Models\Photographer');
    }

}
