<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photographer_rating extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating photographer_rating
   * @var arrays
   */
    protected $fillable = ['customer_id', 'photographer_id', 'rating', 'date'];

/**
 * one to many relation between photographer_rating and customer
 * @return instance of customer model
 */
    public function customers() {
      return $this->belongsTo('App\Models\Customer');
    }

/**
 * one to many relation between photographer and photographer_rating model
 * @return instance of photographer model
 */
    public function photographers() {
      return $this->belongsTo('App\Models\Photographer');
    }
}
