<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
  /**
   * arrays that are mass assignable when inserting/updating genre model
   * @var array
   */
    protected $fillable = ['name'];

/**
 * one to many relation between genre and genre_photographer
 * @return instance of genre_photographer
 */
    public function genrePhotographers() {
      return $this->hasMany('App\Models\Genre_photographer');
    }

/**
 * one to many relation between genre and photographer_price
 * @return instance of photographer_price model
 */
    public function photographerPrices() {
      return $this->hasMany('App\Models\Photographer_price');
    }
}
