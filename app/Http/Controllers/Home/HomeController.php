<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

  /**
   * load the dashboard page
   * @return View [description]
   */
    public function getIndex() {
      return view('frontend.home.main_home');
    }
}
