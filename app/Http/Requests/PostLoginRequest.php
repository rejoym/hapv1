<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-username' => 'required',
            'input-password' => 'required'
        ];
    }

    public function messages() {
        return [
            'input-username.required' => 'Username or email required',
            'input-password.required' => 'Password is required'
        ];
    }
}
