<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotographersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photographers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 255);
            $table->string('middle_name', 255);
            $table->string('last_name', 255);
            $table->string('country', 255);
            $table->string('city', 255);
            $table->string('address', 255);
            $table->enum('gender', ['male', 'female', 'others']);
            $table->string('contact_number', 255);
            $table->string('email', 255);
            $table->date('date_of_birth');
            $table->string('identity_pic', 255);
            $table->string('award_pic', 255);
            $table->string('profile_pic', 255);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('payment_info', 255);
            $table->string('bank_info', 255);
            $table->string('bank_account_no', 255);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('photographers', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photographers');
    }
}
