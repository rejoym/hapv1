<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAlbumPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_album_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_album_id')->unsigned();
            $table->string('photo_url', 255);
            $table->string('photo_name', 255);
            $table->date('date');
            $table->timestamps();
        });
        Schema::table('customer_album_photos', function (Blueprint $table) {
            $table->foreign('customer_album_id')
                ->references('id')
                ->on('customer_albums')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_album_photos');
    }
}
