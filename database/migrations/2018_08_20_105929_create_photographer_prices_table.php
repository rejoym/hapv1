<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotographerPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photographer_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('genre_id')->unsigned();
            $table->integer('photographer_id')->unsigned();
            $table->integer('shoot_id')->unsigned();
            $table->string('price', 255);
            $table->string('equipments', 255);
            $table->timestamps();
        });
        Schema::table('photographer_prices', function (Blueprint $table) {
            $table->foreign('genre_id')
                ->references('id')
                ->on('genres')
                ->onUpdate('cascade');
            $table->foreign('photographer_id')
                ->references('id')
                ->on('photographers')
                ->onUpdate('cascade');
            $table->foreign('shoot_id')
                ->references('id')
                ->on('shoots')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photographer_prices');
    }
}
